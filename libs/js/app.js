// Javascript Main Game Using Canvas

var canvas = document.getElementById("mainGame");
var contentsRender = canvas.getContext("2d");

var ball = {
	x : canvas.width/2,
	y : canvas.height-30,
	dx: 2,
	dy: -2,
	radius: 10
};
var paddle = {
	height: 10,
	width: 480, // 75
}
paddle.x = (canvas.width - paddle.width) / 2;
var brick = {
	rightPressed : false,
	leftPressed : false,
	rowCount : 3,
	columnCount : 5,
	width : 75,
	height : 20,
	padding : 10,
	offsetTop : 30,
	offsetLeft : 30,
	x: null,
	y: null,
}
var score = 0;
var bricks = [];
for(c=0; c<brick.columnCount; c++) {
    bricks[c] = [];
    for(r=0; r<brick.rowCount; r++) {
        bricks[c][r] = { x: 0, y: 0, status: 1 };
    }
}
function draw() {
	if(ball.y + ball.dy < ball.radius) {
	    ball.dy = -ball.dy;
	} else if(ball.y + ball.dy > canvas.height-ball.radius) {
	    if(ball.x > paddle.x && ball.x < paddle.x + paddle.width) {
	        ball.dy = -ball.dy;
	    }
	    else {
	        alert("GAME OVER");
	        location.reload();
	    }
	}
	if(ball.x + ball.dx > canvas.width-ball.radius || ball.x + ball.dx < ball.radius) {
	    ball.dx = -ball.dx;
	}
	contentsRender.clearRect(0, 0, canvas.width, canvas.height);
	contentsRender.beginPath();
	contentsRender.arc(ball.x, ball.y, ball.radius, 0, Math.PI*2);
	contentsRender.fillStyle = "#0095DD";
	contentsRender.fill();
	contentsRender.closePath();
	ball.x += ball.dx;
	ball.y += ball.dy;
	drawPaddle();
	drawBricks();
	collisionDetection();
	drawScore();
	requestAnimationFrame(draw);
}
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);
function drawBricks() {
    for(c=0; c<brick.columnCount; c++) {
        for(r=0; r<brick.rowCount; r++) {
            if(bricks[c][r].status == 1) {
                brick.x = (c*(brick.width+brick.padding))+brick.offsetLeft;
                brick.y = (r*(brick.height+brick.padding))+brick.offsetTop;
                bricks[c][r].x = brick.x;
                bricks[c][r].y = brick.y;
                contentsRender.beginPath();
                contentsRender.rect(brick.x, brick.y, brick.width, brick.height);
                contentsRender.fillStyle = "#0095DD";
                contentsRender.fill();
                contentsRender.closePath();
            }
        }
    }
}
function collisionDetection() {
    for(c=0; c<brick.columnCount; c++) {
        for(r=0; r<brick.rowCount; r++) {
            var b = bricks[c][r];
            if(b.status == 1) {
                if(ball.x > b.x && ball.x < b.x+brick.width && ball.y > b.y && ball.y < b.y+brick.height) {
                    ball.dy = -ball.dy;
                    b.status = 0;
                    score ++
                    if(score == brick.rowCount*brick.columnCount) {
                        alert("YOU WIN, CONGRATULATIONS!");
                        location.reload();
                    };
                    if(score == brick.rowCount*brick.columnCount) {
                        alert("YOU WIN, CONGRATULATIONS!");
                        document.location.reload();
                    }
                }
            }
        }
    }
}
function drawPaddle() {
	if(brick.rightPressed && paddle.x < canvas.width-paddle.width) {
	    paddle.x += 7;
	}
	else if(brick.leftPressed && paddle.x > 0) {
	    paddle.x -= 7;
	}
    contentsRender.beginPath();
    contentsRender.rect(paddle.x, canvas.height-paddle.height, paddle.width, paddle.height);
    contentsRender.fillStyle = "#0095DD";
    contentsRender.fill();
    contentsRender.closePath();
}
function keyDownHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = true;
    }
    else if(e.keyCode == 37) {
        leftPressed = true;
    }
}

function keyUpHandler(e) {
    if(e.keyCode == 39) {
        rightPressed = false;
    }
    else if(e.keyCode == 37) {
        leftPressed = false;
    }
}
function drawScore() {
    contentsRender.font = "16px Arial";
    contentsRender.fillStyle = "#0095DD";
    contentsRender.fillText("Score: "+score, 8, 20);
}

draw();